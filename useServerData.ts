
class WebSocketServer {
    init() {
    
    }
    watch(remoteFunction: string) {
        return {
            onChange: this.onChange.bind(this, remoteFunction),
            dispose: this.dispose(this, remoteFunction),
        };
    }
}

const useServerData = <T,>(remoteFunction: string, parameters: any[]) => {
    const server = useContext(WebSocketServerContext);
    const [serverData, setServerData] = useState(null);
    const watch = useRef(server.watch(remoteFunction));
    useEffect(() => {
        watch.current.onChange((newValue) => {
            setServerData(newValue);
        });
        return () => {
            watch.current.dispose();
        };
    });
    return serverData;
};
