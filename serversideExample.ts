
const notifications = (params, sendChange) => {
    sql.listen("notifications", async () => {
        const d = await sql.query("SELECT * FROM Notifications");
        sendChange(d);
    });
    return () => {
        sql.unlisten();
    };
};

webSocket.register("notifications", notifications, {
    permissions: BasicPermissions,
});
